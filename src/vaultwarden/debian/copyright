Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: vaultwarden
Upstream-Contact: Daniel García <dani1861994@hotmail.com>
Source: https://github.com/dani-garcia/vaultwarden

Files: *
Copyright: 2018-2023 Daniel García <dani1861994@hotmail.com>
License: GPL-3

Files: src/static/scripts/bootstrap-native.js
       src/static/scripts/bootstrap.css
       src/static/scripts/datatables.js
       src/static/scripts/jdenticon.js
       src/static/scripts/jquery-3.6.2.slim.js
Copyright: 2008-2022 SpryMedia Ltd
           2011-2022 The Bootstrap Authors
           2011-2022 Twitter, Inc.
           2014-2021 Daniel Mester Pirttijärvi
           2015-2022 dnp_theme
           2022      OpenJS Foundation and other contributors
License: MIT

Files: debian/*
Copyright: 2023 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
           2023 Guilherme de Paula Xavier Segundo <guilherme.lnx@gmail.com>
License: GPL-3

License: GPL-3
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
Comment:
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
