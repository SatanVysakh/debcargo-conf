This patch on reverting some of the changes from upstream commit
191ade2c1f2ec27a6b8d7dc9d7e3881335ba4e91

It is needed because Debian currently only has rustc 1.70, which does not
implement the sync trait on std::sync::mpsc::channel.

See discussion at https://github.com/rayon-rs/rayon/issues/1106

Index: rayon-core/Cargo.toml
===================================================================
--- rayon-core.orig/Cargo.toml
+++ rayon-core/Cargo.toml
@@ -68,6 +68,9 @@ version = "0.8.1"
 [dependencies.crossbeam-utils]
 version = "0.8.0"
 
+[dev-dependencies.crossbeam-channel]
+version = "0.5"
+
 [dev-dependencies.rand]
 version = "0.8"
 
Index: rayon-core/src/broadcast/test.rs
===================================================================
--- rayon-core.orig/src/broadcast/test.rs
+++ rayon-core/src/broadcast/test.rs
@@ -2,7 +2,6 @@
 
 use crate::ThreadPoolBuilder;
 use std::sync::atomic::{AtomicUsize, Ordering};
-use std::sync::mpsc::channel;
 use std::sync::Arc;
 use std::{thread, time};
 
@@ -15,7 +14,7 @@ fn broadcast_global() {
 #[test]
 #[cfg_attr(any(target_os = "emscripten", target_family = "wasm"), ignore)]
 fn spawn_broadcast_global() {
-    let (tx, rx) = channel();
+    let (tx, rx) = crossbeam_channel::unbounded();
     crate::spawn_broadcast(move |ctx| tx.send(ctx.index()).unwrap());
 
     let mut v: Vec<_> = rx.into_iter().collect();
@@ -34,7 +33,7 @@ fn broadcast_pool() {
 #[test]
 #[cfg_attr(any(target_os = "emscripten", target_family = "wasm"), ignore)]
 fn spawn_broadcast_pool() {
-    let (tx, rx) = channel();
+    let (tx, rx) = crossbeam_channel::unbounded();
     let pool = ThreadPoolBuilder::new().num_threads(7).build().unwrap();
     pool.spawn_broadcast(move |ctx| tx.send(ctx.index()).unwrap());
 
@@ -54,7 +53,7 @@ fn broadcast_self() {
 #[test]
 #[cfg_attr(any(target_os = "emscripten", target_family = "wasm"), ignore)]
 fn spawn_broadcast_self() {
-    let (tx, rx) = channel();
+    let (tx, rx) = crossbeam_channel::unbounded();
     let pool = ThreadPoolBuilder::new().num_threads(7).build().unwrap();
     pool.spawn(|| crate::spawn_broadcast(move |ctx| tx.send(ctx.index()).unwrap()));
 
@@ -82,7 +81,7 @@ fn broadcast_mutual() {
 #[test]
 #[cfg_attr(any(target_os = "emscripten", target_family = "wasm"), ignore)]
 fn spawn_broadcast_mutual() {
-    let (tx, rx) = channel();
+    let (tx, rx) = crossbeam_channel::unbounded();
     let pool1 = Arc::new(ThreadPoolBuilder::new().num_threads(3).build().unwrap());
     let pool2 = ThreadPoolBuilder::new().num_threads(7).build().unwrap();
     pool1.spawn({
@@ -119,7 +118,7 @@ fn broadcast_mutual_sleepy() {
 #[test]
 #[cfg_attr(any(target_os = "emscripten", target_family = "wasm"), ignore)]
 fn spawn_broadcast_mutual_sleepy() {
-    let (tx, rx) = channel();
+    let (tx, rx) = crossbeam_channel::unbounded();
     let pool1 = Arc::new(ThreadPoolBuilder::new().num_threads(3).build().unwrap());
     let pool2 = ThreadPoolBuilder::new().num_threads(7).build().unwrap();
     pool1.spawn({
@@ -159,8 +158,8 @@ fn broadcast_panic_one() {
 #[test]
 #[cfg_attr(not(panic = "unwind"), ignore)]
 fn spawn_broadcast_panic_one() {
-    let (tx, rx) = channel();
-    let (panic_tx, panic_rx) = channel();
+    let (tx, rx) = crossbeam_channel::unbounded();
+    let (panic_tx, panic_rx) = crossbeam_channel::unbounded();
     let pool = ThreadPoolBuilder::new()
         .num_threads(7)
         .panic_handler(move |e| panic_tx.send(e).unwrap())
@@ -197,8 +196,8 @@ fn broadcast_panic_many() {
 #[test]
 #[cfg_attr(not(panic = "unwind"), ignore)]
 fn spawn_broadcast_panic_many() {
-    let (tx, rx) = channel();
-    let (panic_tx, panic_rx) = channel();
+    let (tx, rx) = crossbeam_channel::unbounded();
+    let (panic_tx, panic_rx) = crossbeam_channel::unbounded();
     let pool = ThreadPoolBuilder::new()
         .num_threads(7)
         .panic_handler(move |e| panic_tx.send(e).unwrap())
@@ -232,7 +231,7 @@ fn broadcast_sleep_race() {
 
 #[test]
 fn broadcast_after_spawn_broadcast() {
-    let (tx, rx) = channel();
+    let (tx, rx) = crossbeam_channel::unbounded();
 
     // Queue a non-blocking spawn_broadcast.
     crate::spawn_broadcast(move |ctx| tx.send(ctx.index()).unwrap());
@@ -248,7 +247,7 @@ fn broadcast_after_spawn_broadcast() {
 
 #[test]
 fn broadcast_after_spawn() {
-    let (tx, rx) = channel();
+    let (tx, rx) = crossbeam_channel::unbounded();
 
     // Queue a regular spawn on a thread-local deque.
     crate::registry::in_worker(move |_, _| {
