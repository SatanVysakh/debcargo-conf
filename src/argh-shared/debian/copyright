Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: argh_shared
Upstream-Contact:
 Taylor Cramer <cramertj@google.com>
 Benjamin Brittain <bwb@google.com>
 Erick Tryzelaar <etryzelaar@google.com>
Source: https://github.com/google/argh

Files: *
Copyright:
 2020-2022 Taylor Cramer <cramertj@google.com>
 2020-2022 Benjamin Brittain <bwb@google.com>
 2020-2022 Erick Tryzelaar <etryzelaar@google.com>
 2019 The Fuchsia Authors
License: BSD-3-Clause

Files: src/lib.rs
Copyright: 2020 Google LLC All rights reserved.
License: BSD-3-Clause

Files: debian/*
Copyright:
 2022 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2022 Matthias Geiger <matthias.geiger1024@tutanota.de>
License: BSD-3-Clause

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 3. Neither the name of the copyright holder nor the names of its contributors
 may be used to endorse or promote products derived from this software without
 specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
